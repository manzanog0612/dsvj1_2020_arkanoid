#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace game
{
	namespace audio
	{
		extern Sound click;
		extern Sound bricksRebound;
		extern Sound padRebound;
		extern Sound enter;
		extern Sound youLose;
		extern Sound losing;
		extern Sound youWin;
		extern Sound winning;
		extern Music gameplayStream;
		extern Music menuStream;

		extern bool soundPlayed;
		extern bool sound2Played;

		void loadAudio();

		void playSound();
		void playMusic();
	}
}

#endif // AUDIO_H
