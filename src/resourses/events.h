#ifndef EVENTS_H
#define EVENTS_H

namespace game
{
	namespace events
	{
		extern bool selectOption;
		extern bool bricksCollision;
		extern bool padCollision;
		extern bool enterPressed;
		extern bool losingMatch;
		extern bool losingMatch2;
		extern bool winningMatch;
		extern bool winningMatch2;
		extern bool inMenu;
		extern bool inGameplay;
		extern bool mute;
	}
}

#endif // EVENTS_H