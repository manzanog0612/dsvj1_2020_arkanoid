#include "resourses/textures.h"

#include "raylib.h"

namespace game
{
	namespace textures
	{
		Textures menuBackground;
		Textures gameplayBackground;
		Textures lives;
		Textures brick_green;
		Textures brick_purple;
		Textures brick_blue;
		Textures brick_orange;

		void loadTextures()
		{
			//textures
			menuBackground.texture = LoadTexture("res/assets/textures/menuBackground.png");
			gameplayBackground.texture = LoadTexture("res/assets/textures/gameplayBackground.png");
			lives.texture = LoadTexture("res/assets/textures/lives.png");
			brick_green.texture = LoadTexture("res/assets/textures/brick_green.png");
			brick_purple.texture = LoadTexture("res/assets/textures/brick_purple.png");
			brick_blue.texture = LoadTexture("res/assets/textures/brick_blue.png");
			brick_orange.texture = LoadTexture("res/assets/textures/brick_orange.png");

			//width
			menuBackground.width = 1000;
			gameplayBackground.width = 1000;
			lives.width = 206;
			brick_green.width = 800;
			brick_purple.width = 800;
			brick_blue.width = 800;
			brick_orange.width = 800;

			//height
			menuBackground.height = 1200;
			gameplayBackground.height = 1200;
			lives.height = 206;
			brick_green.height = 200;
			brick_purple.height = 200;
			brick_blue.height = 200;
			brick_orange.height = 200;
		}
	}
}