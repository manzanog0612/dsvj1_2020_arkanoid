#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"

namespace game
{
	namespace textures
	{
		struct Textures
		{
			Texture2D texture;
			int width;
			int height;
		};

		extern Textures menuBackground;
		extern Textures gameplayBackground;
		extern Textures lives;
		extern Textures brick_green;
		extern Textures brick_purple;
		extern Textures brick_blue;
		extern Textures brick_orange;

		void loadTextures();
	}
}

#endif // TEXTURES_H
