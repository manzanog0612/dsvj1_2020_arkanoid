#include "resourses/audio.h"

#include "resourses/events.h"

using namespace game;
using namespace events;

namespace game
{
	namespace audio
	{
		Sound click;
		Sound bricksRebound;
		Sound padRebound;
		Sound enter;
		Sound youLose;
		Sound losing;
		Sound youWin;
		Sound winning;
		Music gameplayStream;
		Music menuStream;

		bool soundPlayed = false;
		bool sound2Played = false;
		
		void loadAudio()
		{
			click = LoadSound("res/assets/audio/sounds/click.mp3");
			bricksRebound = LoadSound("res/assets/audio/sounds/bricksRebound.mp3");
			padRebound = LoadSound("res/assets/audio/sounds/padRebound.mp3");
			enter = LoadSound("res/assets/audio/sounds/enter.mp3");
			youLose = LoadSound("res/assets/audio/sounds/youLose.mp3");
			losing = LoadSound("res/assets/audio/sounds/losing.mp3");
			youWin = LoadSound("res/assets/audio/sounds/youWin.mp3");
			winning = LoadSound("res/assets/audio/sounds/winning.mp3");
			gameplayStream = LoadMusicStream("res/assets/audio/music/gameplay.mp3");
			menuStream = LoadMusicStream("res/assets/audio/music/menu.mp3");
		}

		void playSound()
		{
			if (!mute && selectOption) PlaySoundMulti(click);
			if (!mute && bricksCollision) PlaySoundMulti(bricksRebound);
			if (!mute && padCollision) PlaySoundMulti(padRebound);
			if (!mute && enterPressed) PlaySoundMulti(enter);
			if (!mute && losingMatch) PlaySound(losing);
			if (!mute && losingMatch2) PlaySound(youLose);
			if (!mute && winningMatch) PlaySound(winning);
			if (!mute && winningMatch2) PlaySound(youWin);

			SetSoundVolume(click, 0.5f);
			SetSoundVolume(bricksRebound, 0.5f);
			SetSoundVolume(padRebound, 0.1f);
			SetSoundVolume(enter, 0.5f);
			SetSoundVolume(losing, 0.1f);
			SetSoundVolume(youLose, 0.8f);
			SetSoundVolume(winning, 0.1f);
			SetSoundVolume(youWin, 0.8f);
		}

		void playMusic()
		{
			if (inMenu) PlayMusicStream(menuStream);
			if (inGameplay) PlayMusicStream(gameplayStream);

			SetMusicVolume(menuStream, 0.1f);
			SetMusicVolume(gameplayStream, 0.1f);
		}
	}
}