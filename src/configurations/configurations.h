#ifndef CONFIGURATIONS_H
#define CONFIGURATIONS_H

namespace configurations
{
	struct Mouse
	{
		int posX;
		int posY;
	};

	enum class PLACEINGAME { MENU, GAMEPLAY, RULES, CREDITS, EXIT, NONE };
	enum class PLACEINOPTIONS { CONTROLS, PADDLECOLOR, BALLCOLOR, GAMEMODE, NONE };
	enum class LEVELS { ONE, TWO, THREE };

	const int screenWidth = 500;
	const int screenHeight = 600;

	struct ScreenLimit
	{
		int right = screenWidth;
		int left = 0;
		int up = 0;
		int down = screenHeight;
	};

	extern ScreenLimit screenLimit;
	extern LEVELS actualLevel;
}

#endif //CONFIGURATIONS_H
