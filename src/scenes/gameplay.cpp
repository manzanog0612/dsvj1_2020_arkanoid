#include "scenes/gameplay.h"

#include <cmath>

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "elements/elements.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace elements;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace gameplay
	{
		static void playerAndBallDefinitions()
		{
			player.pad.height = static_cast<float>(20 * drawScaleY);
			player.pad.width = static_cast<float>(100 * drawScaleX);
			player.pad.x = static_cast<float>((screenWidth - player.pad.width) / 2);
			player.pad.y = static_cast<float>(screenLimit.down - player.pad.height - contourLineThickness);
			player.color = RED;
			player.speed = static_cast<int>(7 * drawScaleX);
			player.lives = 3;

			player.controls.left = KEY_A;
			player.controls.right = KEY_D;

			ball.radius = 10.0f * drawScaleY;
			ball.center.x = static_cast<float>(player.pad.x + player.pad.width / 2);
			ball.center.y = static_cast<float>(player.pad.y - ball.radius - 1);
			ball.speed.x = 0.0f;
			ball.speed.y = static_cast<float>(-7 * drawScaleY);
			ball.state.actual = STATEOFBALL::NOTONCOLLISION;
			ball.state.previous = STATEOFBALL::NOTONCOLLISION;
			ball.color = YELLOW;
		}

		static void defineLevel1()
		{
			for (short i = 0; i < bricks_::linesOfBricks; i++)
			{
				for (short j = 0; j < bricks_::bricksPerLine; j++)
				{
					switch (i)
					{
					case 0:
					case 4:
					case 6:
						bricks[i][j].active = true;
						bricks[i][j].type = TYPESOFBRIKS::BREAKABLE;
						bricks[i][j].color = GREEN;
						bricks[i][j].hits = 2;			break;
					case 1:
					case 3:
					case 7:
						bricks[i][j].active = true;
						bricks[i][j].type = TYPESOFBRIKS::BREAKABLE;
						bricks[i][j].color = SKYBLUE;
						bricks[i][j].hits = 1;			break;
					default:
						bricks[i][j].active = false;
						bricks[i][j].color = BLACK;
						bricks[i][j].type = TYPESOFBRIKS::NONEXISTENT; break;
					}
					
					if (i == 2 || i == 5)
					{
						switch (j)
						{
						case 1:
						case 3:
							if (bricks[i][j].active)
								bricks[i][j].active = false;
							bricks[i][j].hits = 5;
							bricks[i][j].type = TYPESOFBRIKS::UNBREAKABLE;
							bricks[i][j].color = VIOLET; break;
						default:break;
						}
					}		
				}
			}
		}

		static void defineLevel2()
		{
			for (short i = 0; i < bricks_::linesOfBricks; i++)
			{
				for (short j = 0; j < bricks_::bricksPerLine; j++)
				{
					switch (i)
					{
					case 0:
					case 2:
						bricks[i][j].active = true;
						bricks[i][j].type = TYPESOFBRIKS::BREAKABLE;
						bricks[i][j].color = GREEN;
						bricks[i][j].hits = 2;			break;
					case 4:
					case 6:
					case 7:
						bricks[i][j].active = true;
						bricks[i][j].type = TYPESOFBRIKS::BREAKABLE;
						bricks[i][j].color = SKYBLUE;
						bricks[i][j].hits = 1;			break;
					default:
						if (bricks[i][j].active)
							bricks[i][j].active = false;
						bricks[i][j].hits = 5;
						bricks[i][j].type = TYPESOFBRIKS::UNBREAKABLE;
						bricks[i][j].color = VIOLET;	break;
					}

					if (((i % 2 == 0 && j % 2 != 0) || (i % 2 != 0 && j % 2 == 0)) && i != 7 && i != 0)
					{
						if (bricks[i][j].active)
							bricks[i][j].active = false;
						bricks[i][j].color = BLACK;
						bricks[i][j].type = TYPESOFBRIKS::NONEXISTENT;
					}
				}
			}
		}

		static void defineLevel3()
		{
			for (short i = 0; i < bricks_::linesOfBricks; i++)
			{
				for (short j = 0; j < bricks_::bricksPerLine; j++)
				{
					switch (i)
					{
					case 0:
					case 1:
						bricks[i][j].active = true;
						bricks[i][j].type = TYPESOFBRIKS::BREAKABLE; 
						bricks[i][j].color = ORANGE;
						bricks[i][j].hits = 3;			break;
					case 2:
					case 3:
					case 4:
						bricks[i][j].active = true;
						bricks[i][j].type = TYPESOFBRIKS::BREAKABLE;
						bricks[i][j].color = GREEN;
						bricks[i][j].hits = 2;			break;
					case 5:
					case 6:
					case 7:
						bricks[i][j].active = true;
						bricks[i][j].type = TYPESOFBRIKS::BREAKABLE;
						bricks[i][j].color = SKYBLUE;
						bricks[i][j].hits = 1;			break;
					default:break;
					}
				}
			}
		}
		
		static void setBricksDefinitions()
		{
			for (short i = 0; i < bricks_::linesOfBricks; i++)
			{
				for (short j = 0; j < bricks_::bricksPerLine; j++)
				{
					bricks[i][j].brick.height = static_cast<float>(20 * drawScaleY);
					bricks[i][j].brick.width = static_cast<float>(bricks[i][j].brick.height * 4 * drawScaleX);

					bricks[i][j].brick.x = static_cast<float>(contourLineThickness + static_cast<int>(10 * drawScaleX) +
						j * (bricks[i][j].brick.width + static_cast<int>(14 * drawScaleX)));
					bricks[i][j].brick.y = static_cast<float>(screenLimit.down / 12 + contourLineThickness / 2 + 7 +
						i * (bricks[i][j].brick.height + static_cast<int>(5 * drawScaleY)));
				}
			}

			if (actualLevel == LEVELS::ONE)
			{
				defineLevel1();
			}

			if (actualLevel == LEVELS::TWO)
			{
				defineLevel2();
			}

			if (actualLevel == LEVELS::THREE)
			{
				defineLevel3();
			}
		}

		static void restartGame()
		{
			player.pad.x = static_cast<float>((screenWidth - player.pad.width) / 2);
			player.pad.y = static_cast<float>(screenLimit.down - player.pad.height - contourLineThickness);
			player.lives = 3;
			player.score = 0;

			ball.center.x = static_cast<float>(player.pad.x + player.pad.width / 2);
			ball.center.y = static_cast<float>(player.pad.y - ball.radius - 1);
			ball.speed.x = 0;
			ball.speed.y = static_cast<float>(-7 * drawScaleY);
			ball.state.actual = STATEOFBALL::NOTONCOLLISION;
			ball.state.previous = STATEOFBALL::NOTONCOLLISION;
			ball.active = false;

			actualLevel = LEVELS::ONE;
			if (win) win = false;
			setBricksDefinitions();

			if (soundPlayed)
				soundPlayed = false;
			if (sound2Played)
				sound2Played = false;
		}

		static void restartMatch()
		{
			player.pad.x = static_cast<float>((screenWidth - player.pad.width) / 2);
			player.pad.y = static_cast<float>(screenLimit.down - player.pad.height - contourLineThickness);

			ball.center.x = static_cast<float>(player.pad.x + player.pad.width / 2);
			ball.center.y = static_cast<float>(player.pad.y - ball.radius - 1);
			ball.speed.x = 0;
			ball.speed.y = static_cast<float>(-7 * drawScaleY);
			ball.state.actual = STATEOFBALL::NOTONCOLLISION;
			ball.state.previous = STATEOFBALL::NOTONCOLLISION;
			ball.active = false;

			if (soundPlayed)
				soundPlayed = false;
			if (sound2Played)
				sound2Played = false;
		}

		static int addScore()
		{
			return 5;
		}

		static void setScore(int score)
		{
			player.score += score;
		}

		static void setSoundCollisionBrick()
		{
			bricksCollision = true;
			soundPlayed = true;
		}

		static void setSoundCollisionPad()
		{
			padCollision = true;
			soundPlayed = true;
		}

		static void setBallBricksCollision()
		{
			float px; // point of the brick in x most proximate to ball;
			float py; // point of the brick in y most proximate to ball;
			float distance;
			bool collision = false;

			bool collisionSetX = false;
			bool collisionSetY = false;

			for (short i = 0; i < bricks_::linesOfBricks; i++)
			{
				for (short j = 0; j < bricks_::bricksPerLine; j++)
				{
					if (collision)
						collision = false;

					px = ball.center.x;
					py = ball.center.y;

					if (px < bricks[i][j].brick.x) px = bricks[i][j].brick.x;
					if (px > bricks[i][j].brick.x + bricks[i][j].brick.width) px = bricks[i][j].brick.x + bricks[i][j].brick.width;

					if (py < bricks[i][j].brick.y) py = bricks[i][j].brick.y;
					if (py > bricks[i][j].brick.y + bricks[i][j].brick.height) py = bricks[i][j].brick.y + bricks[i][j].brick.height;

					distance = sqrt((ball.center.x - px) * (ball.center.x - px) + (ball.center.y - py) * (ball.center.y - py));

					if (distance <= ball.radius) collision = true;

					if (collision && bricks[i][j].type != TYPESOFBRIKS::NONEXISTENT && (bricks[i][j].active || bricks[i][j].type == TYPESOFBRIKS::UNBREAKABLE))
					{
						if (py == bricks[i][j].brick.y || py == bricks[i][j].brick.y + bricks[i][j].brick.height)
						{
							if (bricks[i][j].type == TYPESOFBRIKS::BREAKABLE)
							{
								bricks[i][j].hits--;
								if (bricks[i][j].hits == 0)
								{
									bricks[i][j].active = false;
									setScore(addScore());
								}
							}

							if (!collisionSetY)
							{
								ball.speed.y *= -1;
								collisionSetY = true;
							}

							if ((px == bricks[i][j].brick.x || px == bricks[i][j].brick.x + bricks[i][j].brick.width) && !collisionSetX)
							{
								ball.speed.x *= -1;
								collisionSetX = true;
							}
						}
						else
						{
							if (bricks[i][j].type == TYPESOFBRIKS::BREAKABLE)
							{
								bricks[i][j].hits--;
								if (bricks[i][j].hits == 0)
								{
									bricks[i][j].active = false;
									setScore(addScore());
								}
							}

							if (!collisionSetX)
							{
								ball.speed.x *= -1;
								collisionSetX = true;
							}

							if ((py == bricks[i][j].brick.y || py == bricks[i][j].brick.y + bricks[i][j].brick.height) && !collisionSetY)
							{
								ball.speed.y *= -1;
								collisionSetY = true;
							}
						}
					}
				}
			}
		}

		static bool checkCollisionBallBricks()
		{
			for (short i = 0; i < bricks_::linesOfBricks; i++)
			{
				for (short j = 0; j < bricks_::bricksPerLine; j++)
				{
					if (CheckCollisionCircleRec(ball.center, ball.radius, bricks[i][j].brick) && 
						(bricks[i][j].active || bricks[i][j].type == TYPESOFBRIKS::UNBREAKABLE))
					{
						return true;
					}
				}
			}

			return false;
		}

		static void setPlayerMovement()
		{
			if (padMovement == -1 && player.pad.x - player.speed >= screenLimit.left + contourLineThickness)
				player.pad.x -= player.speed * 50 * GetFrameTime();
			if (padMovement == 1 && player.pad.x + player.pad.width + player.speed <= screenLimit.right - contourLineThickness)
				player.pad.x += player.speed * 50 * GetFrameTime();
		}

		static void setBallMovement()
		{
			ball.state.actual = STATEOFBALL::NOTONCOLLISION;

			if (CheckCollisionCircleRec(ball.center, ball.radius, player.pad) || checkCollisionBallBricks() /*|| 
				((ball.center.x + ball.radius) >= screenLimit.right - contourLineThickness) ||
				((ball.center.x - ball.radius) <= screenLimit.left + contourLineThickness) || 
				(ball.center.y - ball.radius) <= screenLimit.down / 12 + contourLineThickness / 2*/)
				ball.state.actual = STATEOFBALL::ONCOLLISION;

			if (ball.state.previous == STATEOFBALL::ONCOLLISION && ball.state.actual == STATEOFBALL::ONCOLLISION)
				ball.state.actual = STATEOFBALL::NOTONCOLLISION;
			else
				ball.state.previous = ball.state.actual;

			switch (ball.state.actual)
			{
			case STATEOFBALL::ONCOLLISION:
				//player's pad
				if (CheckCollisionCircleRec(ball.center, ball.radius, player.pad))
				{
					if ((ball.center.x + ball.radius <= player.pad.x || ball.center.x - ball.radius >= player.pad.x + player.pad.width) && 
						ball.center.y + ball.radius >= player.pad.y)
					{
						/*ball.speed.y *= -1;
						ball.speed.x = (ball.center.x - (player.pad.x + player.pad.width / 2)) / (player.pad.width / 2) * player.speed;*/ball.speed.x *= -1;
					}
					else
					{
						ball.speed.y *= -1;
						ball.speed.x = (ball.center.x - (player.pad.x + player.pad.width / 2)) / (player.pad.width / 2) * player.speed;
					}
					if (!soundPlayed)
						setSoundCollisionBrick();
				}
				//bricks
				if (checkCollisionBallBricks())
				{
					setBallBricksCollision();
					if (!soundPlayed)
						setSoundCollisionPad();
				}
				break;
			case STATEOFBALL::NOTONCOLLISION:
				//walls
				if (((ball.center.x + ball.radius) >= screenLimit.right - contourLineThickness) ||
					((ball.center.x - ball.radius) <= screenLimit.left + contourLineThickness))
				{
					ball.speed.x *= -1;
					if (!soundPlayed)
						setSoundCollisionPad();
				}
				if ((ball.center.y - ball.radius) <= screenLimit.down / 12 + contourLineThickness / 2)
				{
					ball.speed.y *= -1;
					if (!soundPlayed)
						setSoundCollisionPad();
				}
				//down limit
				if ((ball.center.y + ball.radius) >= screenLimit.down - contourLineThickness)
				{
					ball.active = false;
					player.lives--;
					restartMatch();
				}
				ball.center.x += ball.speed.x * 50 * GetFrameTime();
				ball.center.y += ball.speed.y * 50 * GetFrameTime();
				if (soundPlayed)
					soundPlayed = false;
				break;
			default:
				break;
			}
		}

		static bool checkIfAllBricksDestroyed()
		{
			for (short i = 0; i < bricks_::linesOfBricks; i++)
			{
				for (short j = 0; j < bricks_::bricksPerLine; j++)
				{
					if (bricks[i][j].active)
					{
						return false;
					}
				}
			}

			return true;
		}

		static void drawGameContourn(Color color)
		{
			struct Line
			{
				Vector2 StartPos;
				Vector2 EndPos;
			};

			Line upperLine;
			Line upperLine2;
			Line lowerLine;
			Line leftLine;
			Line rightLine;

			upperLine.StartPos.y = static_cast<float>(screenLimit.up + contourLineThickness / 2);
			upperLine.StartPos.x = static_cast<float>(screenLimit.left);
			upperLine.EndPos.y = static_cast<float>(screenLimit.up + contourLineThickness / 2);
			upperLine.EndPos.x = static_cast<float>(screenLimit.right);
			//--------------------------------------------
			upperLine2.StartPos.y = static_cast<float>(screenLimit.down / 12);
			upperLine2.StartPos.x = static_cast<float>(screenLimit.left);
			upperLine2.EndPos.y = static_cast<float>(screenLimit.down / 12);
			upperLine2.EndPos.x = static_cast<float>(screenLimit.right);
			//--------------------------------------------
			lowerLine.StartPos.y = static_cast<float>(screenLimit.down - contourLineThickness / 2);
			lowerLine.StartPos.x = static_cast<float>(screenLimit.left);
			lowerLine.EndPos.y = static_cast<float>(screenLimit.down - contourLineThickness / 2);
			lowerLine.EndPos.x = static_cast<float>(screenLimit.right);
			//--------------------------------------------
			leftLine.StartPos.y = static_cast<float>(screenLimit.up);
			leftLine.StartPos.x = static_cast<float>(screenLimit.left + contourLineThickness / 2);
			leftLine.EndPos.y = static_cast<float>(screenLimit.down);
			leftLine.EndPos.x = static_cast<float>(screenLimit.left + contourLineThickness / 2);
			//--------------------------------------------
			rightLine.StartPos.y = static_cast<float>(screenLimit.up);
			rightLine.StartPos.x = static_cast<float>(screenLimit.right - contourLineThickness / 2);
			rightLine.EndPos.y = static_cast<float>(screenLimit.down);
			rightLine.EndPos.x = static_cast<float>(screenLimit.right - contourLineThickness / 2);
			//--------------------------------------------

			DrawLineEx(upperLine.StartPos, upperLine.EndPos, contourLineThickness, color);
			DrawLineEx(upperLine2.StartPos, upperLine2.EndPos, contourLineThickness, color);
			DrawLineEx(lowerLine.StartPos, lowerLine.EndPos, contourLineThickness, color);
			DrawLineEx(leftLine.StartPos, leftLine.EndPos, contourLineThickness, color);
			DrawLineEx(rightLine.StartPos, rightLine.EndPos, contourLineThickness, color);
		}

		static void drawScore()
		{
			short posX = screenLimit.left + contourLineThickness + 3;
			score.text = "Score: ";
			score.posY = screenLimit.up + contourLineThickness + 3;
			score.fontSize = static_cast<int>(30 * drawScaleY);

			DrawText(score.text, posX, score.posY, score.fontSize, MAGENTA);
			DrawText(FormatText("%i", player.score), posX + MeasureText(score.text, score.fontSize), score.posY, score.fontSize, MAGENTA);
		}

		static void drawPauseScreen()
		{
			pause.text = "Pause";
			pressEnter.text = "Press enter to go back to menu";
			pressP.text = "Press P to coninue";
			pressM.text = "Press M to mute audio";

			pause.fontSize = static_cast<int>(75 * drawScaleY);
			pressEnter.fontSize = static_cast<int>(25 * drawScaleY);
			pressP.fontSize = pressEnter.fontSize;
			pressM.fontSize = pressEnter.fontSize;

			pause.posY = screenLimit.down / 2 - pause.fontSize / 2;
			pressEnter.posY = screenLimit.down - contourLineThickness - 10 - pressEnter.fontSize * 2;
			pressP.posY = screenLimit.down / 12 + contourLineThickness + 10;
			pressM.posY = pressEnter.posY - pressM.fontSize * 2 - pressM.fontSize / 2;

			drawPhraseRectangle(pause, MAGENTA);
			DrawText(pause.text, (screenLimit.right - MeasureText(pause.text, pause.fontSize)) / 2, pause.posY, pause.fontSize, GREEN);

			drawPhraseRectangle(pressEnter, GREEN);
			DrawText(pressEnter.text, (screenLimit.right - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, MAGENTA);

			drawPhraseRectangle(pressP, GREEN);
			DrawText(pressP.text, (screenLimit.right - MeasureText(pressP.text, pressP.fontSize)) / 2, pressP.posY, pressP.fontSize, MAGENTA);

			drawPhraseRectangle(pressM, GREEN);
			DrawText(pressM.text, (screenLimit.right - MeasureText(pressM.text, pressM.fontSize)) / 2, pressM.posY, pressM.fontSize, MAGENTA);
		}

		static void drawPlayersLives()
		{
			const short playersLives = 3;
			Rectangle rec[playersLives];

			for (short i = 0; i < playersLives; i++)
			{
				rec[i].height = static_cast<float>(20 * drawScaleY);
				rec[i].width = static_cast<float>(20 * drawScaleX);
				rec[i].y = static_cast<float>(screenLimit.up + contourLineThickness + 6);
				rec[i].x = static_cast<float>(screenLimit.right - contourLineThickness * 3 - 10 - i * (rec[i].width + 10));
			}

			for (short i = 0; i < player.lives; i++)
			{
				DrawTextureEx(lives.texture, {rec[i].x, rec[i].y}, 0.0f, 0.1f, WHITE);
			}
		}

		static void drawFirsIndications()
		{
			pressEnter.text = "Press enter to go back to menu";
			pressP.text = "Press P to pause";
			pressR.text = "Press R to restart the game";
			pressSpace.text = "Press space to trow the ball";

			pressEnter.fontSize = static_cast<int>(25 * drawScaleY);
			pressP.fontSize = pressEnter.fontSize;
			pressR.fontSize = pressEnter.fontSize;
			pressSpace.fontSize = pressEnter.fontSize + static_cast<int>(5 * drawScaleY);

			pressEnter.posY = screenLimit.down - contourLineThickness - 10 - pressEnter.fontSize * 4;
			pressP.posY = pressEnter.posY - pressP.fontSize - static_cast<int>(10 * drawScaleY);
			pressR.posY = pressP.posY - pressR.fontSize - static_cast<int>(10 * drawScaleY);
			pressSpace.posY = pressR.posY - pressSpace.fontSize - static_cast<int>(10 * drawScaleY);

			if (!pauseGame)
			{
				drawPhraseRectangle(pressEnter, GREEN);
				DrawText(pressEnter.text, (screenLimit.right - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, MAGENTA);

				drawPhraseRectangle(pressP, GREEN);
				DrawText(pressP.text, (screenLimit.right - MeasureText(pressP.text, pressP.fontSize)) / 2, pressP.posY, pressP.fontSize, MAGENTA);
			}

			drawPhraseRectangle(pressR, GREEN);
			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2, pressR.posY, pressR.fontSize, MAGENTA);

			if (!pauseGame)
			{
				drawPhraseRectangle(pressSpace, MAGENTA);
				DrawText(pressSpace.text, (screenLimit.right - MeasureText(pressSpace.text, pressSpace.fontSize)) / 2, pressSpace.posY, pressSpace.fontSize, GREEN);
			}
		}

		static void drawEndGameContourn(Color color)
		{
			struct Line
			{
				Vector2 StartPos;
				Vector2 EndPos;
			};

			Line upperLine;
			Line lowerLine;
			Line leftLine;
			Line rightLine;

			upperLine.StartPos.y = static_cast<float>(screenLimit.up + contourLineThickness / 2);
			upperLine.StartPos.x = static_cast<float>(screenLimit.left);
			upperLine.EndPos.y = static_cast<float>(screenLimit.up + contourLineThickness / 2);
			upperLine.EndPos.x = static_cast<float>(screenLimit.right);
			//--------------------------------------------
			lowerLine.StartPos.y = static_cast<float>(screenLimit.down - contourLineThickness / 2);
			lowerLine.StartPos.x = static_cast<float>(screenLimit.left);
			lowerLine.EndPos.y = static_cast<float>(screenLimit.down - contourLineThickness / 2);
			lowerLine.EndPos.x = static_cast<float>(screenLimit.right);
			//--------------------------------------------
			leftLine.StartPos.y = static_cast<float>(screenLimit.up);
			leftLine.StartPos.x = static_cast<float>(screenLimit.left + contourLineThickness / 2);
			leftLine.EndPos.y = static_cast<float>(screenLimit.down);
			leftLine.EndPos.x = static_cast<float>(screenLimit.left + contourLineThickness / 2);
			//--------------------------------------------
			rightLine.StartPos.y = static_cast<float>(screenLimit.up);
			rightLine.StartPos.x = static_cast<float>(screenLimit.right - contourLineThickness / 2);
			rightLine.EndPos.y = static_cast<float>(screenLimit.down);
			rightLine.EndPos.x = static_cast<float>(screenLimit.right - contourLineThickness / 2);
			//--------------------------------------------

			DrawLineEx(upperLine.StartPos, upperLine.EndPos, contourLineThickness, color);
			DrawLineEx(lowerLine.StartPos, lowerLine.EndPos, contourLineThickness, color);
			DrawLineEx(leftLine.StartPos, leftLine.EndPos, contourLineThickness, color);
			DrawLineEx(rightLine.StartPos, rightLine.EndPos, contourLineThickness, color);
		}

		static void drawLosingScreen()
		{
			loser[0].text = "You lost";
			loser[1].text = "Wanna try again?";

			loser[0].fontSize = static_cast<int>(70 * drawScaleY);
			loser[1].fontSize = static_cast<int>(50 * drawScaleY);

			loser[0].posY = screenHeight / 2 - screenHeight / 8;
			loser[1].posY = loser[0].posY + loser[1].fontSize + loser[1].fontSize / 2;

			drawEndGameContourn(MAGENTA);

			drawPhraseRectangle(loser[0], GREEN);
			DrawText(loser[0].text, (screenWidth - MeasureText(loser[0].text, loser[0].fontSize)) / 2,
				loser[0].posY, loser[0].fontSize, MAGENTA);

			drawPhraseRectangle(loser[1], GREEN);
			DrawText(loser[1].text, (screenWidth - MeasureText(loser[1].text, loser[1].fontSize)) / 2,
				loser[1].posY, loser[1].fontSize, MAGENTA);

			drawPhraseRectangle(pressR, GREEN);
			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2,
				pressR.posY, pressR.fontSize, MAGENTA);
		}

		static void drawWinnerScreen()
		{
			winner[0].text = "You won";
			winner[1].text = "Congratulations!";

			winner[0].fontSize = static_cast<int>(70 * drawScaleY);
			winner[1].fontSize = static_cast<int>(50 * drawScaleY);

			winner[0].posY = screenHeight / 2 - screenHeight / 8;
			winner[1].posY = winner[0].posY + winner[1].fontSize + winner[1].fontSize / 2;

			drawEndGameContourn(MAGENTA);

			drawPhraseRectangle(winner[0], GREEN);
			DrawText(winner[0].text, (screenWidth - MeasureText(winner[0].text, winner[0].fontSize)) / 2,
				winner[0].posY, winner[0].fontSize, MAGENTA);

			drawPhraseRectangle(winner[1], GREEN);
			DrawText(winner[1].text, (screenWidth - MeasureText(winner[1].text, winner[1].fontSize)) / 2,
				winner[1].posY, winner[1].fontSize, MAGENTA);

			drawPhraseRectangle(pressR, GREEN);
			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2,
				pressR.posY, pressR.fontSize, MAGENTA);
		}

		static bool checkIfAnyBrickLeft()
		{
			for (short i = 0; i < bricks_::linesOfBricks; i++)
			{
				for (short j = 0; j < bricks_::bricksPerLine; j++)
				{
					if (bricks[i][j].active)
						return true;
				}
			}

			return false;
		}

		#if DEBUG
		static void autoWin()
		{
			if (IsKeyPressed(KEY_Q))
			{
				for (short i = 0; i < bricks_::linesOfBricks; i++)
				{
					for (short j = 0; j < bricks_::bricksPerLine; j++)
					{
						if (bricks[i][j].active)
							bricks[i][j].active = false;
						bricks[i][j].type = TYPESOFBRIKS::NONEXISTENT;
					}
				}

				restartMatch();
			}
		}
		#endif

		//------------------------------------------------------------------------------

		void initialization()
		{
			actualLevel = LEVELS::ONE;
			setBricksDefinitions();
			playerAndBallDefinitions();
		}

		void input()
		{
			if (IsKeyPressed(player.controls.left) || IsKeyPressed(player.controls.right) || 
				IsKeyDown(player.controls.left) || IsKeyDown(player.controls.right))
			{
				if (IsKeyPressed(player.controls.left) || IsKeyDown(player.controls.left))
					padMovement = -1;
				if (IsKeyPressed(player.controls.right) || IsKeyDown(player.controls.right))
					padMovement = 1;
			}
			else padMovement = 0;

			if (!ball.active)
			{
				if (IsKeyPressed(KEY_SPACE))
					ball.active = true;
			}

			if (IsKeyPressed(KEY_P)) pauseGame = !pauseGame;

			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_R))
				restart = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			#if DEBUG
			autoWin();
			#endif

			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
				pauseGame = false;
				restartGame();
			}

			if (restart)
			{
				restart = false;
				restartGame();
				SetMusicVolume(gameplayStream, 0.1f);
			}
			
			if (!checkIfAnyBrickLeft())
			{
				switch (actualLevel)
				{
				case configurations::LEVELS::ONE:
					actualLevel = LEVELS::TWO;		break;
				case configurations::LEVELS::TWO:
					actualLevel = LEVELS::THREE;	break;
				case configurations::LEVELS::THREE:
					win = true;						break;
				default:break;
				}
				if(!win)
					restartMatch();
				setBricksDefinitions();
			}

			if (player.lives != 0 && !win)
			{
				if (!pauseGame)
				{
					setPlayerMovement();
					if (ball.active)
						setBallMovement();
					else
					{
						ball.center.x = static_cast<float>(player.pad.x + player.pad.width / 2);
						ball.center.y = static_cast<float>(player.pad.y - ball.radius - 1);
					}
				}
			}
			else if (player.lives == 0)
			{
				SetMusicVolume(gameplayStream, 0.05f);

				if (!soundPlayed)
				{
					losingMatch = true;
					soundPlayed = true;
				}

				if (!IsSoundPlaying(losing) && !losingMatch && !sound2Played)
				{
					losingMatch2 = true;
					sound2Played = true;
				}
				
			}
			else
			{
				SetMusicVolume(gameplayStream, 0.02f);

				if (!soundPlayed)
				{
					winningMatch = true;
					soundPlayed = true;
				}

				if (!IsSoundPlaying(winning) && !winningMatch && !sound2Played)
				{
					winningMatch2 = true;
					sound2Played = true;
				}
			}

			if (!inGameplay)
			{
				inGameplay = true;
				playMusic();
			}
			if (!mute)
			{
				UpdateMusicStream(gameplayStream);
			}
			playSound();
			if (bricksCollision)	bricksCollision = false;
			if (padCollision)		padCollision = false;
			if (losingMatch)		losingMatch = false;
			if (losingMatch2)		losingMatch2 = false;
			if (winningMatch)		winningMatch = false;
			if (winningMatch2)		winningMatch2 = false;
			if (enterPressed)		enterPressed = false;
		}

		void draw()
		{
			ClearBackground(BLACK);
			
			DrawTextureEx(gameplayBackground.texture, { 0.0f, 0.0f }, 0.0f, 
			static_cast<float>(screenWidth + screenHeight) / static_cast<float>(gameplayBackground.width + gameplayBackground.height), WHITE);

			if (player.lives == 0 || win)
			{
				if (player.lives == 0)
					drawLosingScreen();

				if (win)
					drawWinnerScreen();
			}
			else
			{
				drawScore();
				drawPlayersLives();

				if (!ball.active) drawFirsIndications();

				if (!pauseGame)
				{
					drawGameContourn(GREEN);

					for (short i = 0; i < bricks_::linesOfBricks; i++)
					{
						for (short j = 0; j < bricks_::bricksPerLine; j++)
						{
							if (bricks[i][j].active || bricks[i][j].type == TYPESOFBRIKS::UNBREAKABLE)
							{
								switch (bricks[i][j].hits)
								{
								case 3:
									DrawTextureEx(brick_orange.texture, { bricks[i][j].brick.x,  bricks[i][j].brick.y },
										0.0f, static_cast<float>(bricks[i][j].brick.width + bricks[i][j].brick.height) /
										static_cast<float>(brick_orange.width + brick_orange.height), WHITE);
									break;
								case 2:
									DrawTextureEx(brick_green.texture, { bricks[i][j].brick.x,  bricks[i][j].brick.y },
										0.0f, static_cast<float>(bricks[i][j].brick.width + bricks[i][j].brick.height) /
										static_cast<float>(brick_green.width + brick_green.height), WHITE);
									break;
								case 1:
									DrawTextureEx(brick_blue.texture, { bricks[i][j].brick.x,  bricks[i][j].brick.y },
										0.0f, static_cast<float>(bricks[i][j].brick.width + bricks[i][j].brick.height) /
										static_cast<float>(brick_blue.width + brick_blue.height), WHITE);
									break;
								default:
									DrawTextureEx(brick_purple.texture, { bricks[i][j].brick.x,  bricks[i][j].brick.y },
										0.0f, static_cast<float>(bricks[i][j].brick.width + bricks[i][j].brick.height) /
										static_cast<float>(brick_purple.width + brick_purple.height), WHITE);
									break;
								}
							}
							
						}
					}
					DrawCircleV(ball.center, ball.radius, ball.color);
					DrawRectangleRec(player.pad, player.color);

					#if DEBUG
					DrawCircleLines(static_cast<int>(ball.center.x), static_cast<int>(ball.center.y), ball.radius, MAGENTA);
					#endif // DEBUG
				}
				else
				{
					drawGameContourn(MAGENTA);
					drawPauseScreen();
					drawFirsIndications();
				}
			}
			
		}

		void deinitialization()
		{
			
		}
	}
}
