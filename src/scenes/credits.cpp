#include "scenes/credits.h"

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace credits
	{
		void initialization()
		{

		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
			}

			if (!mute)
				UpdateMusicStream(menuStream);
		}

		void draw()
		{
			const short fontSize = static_cast<int>(22 * drawScaleY);
			const Color color = YELLOW;

			creditsText[firstLine].text = "Audio";
			creditsText[secondLine].text = "From the page freesound.org, credits to:";
			creditsText[thirdLine].text = "PhonosUPF, davidou, JonnyRuss01, qubodup,";
			creditsText[fourthLine].text = "Rocotilos, AdamWeeden, bolkmar";
			creditsText[fifthLine].text = "and LittleRobotSoundFactory";
			creditsText[sixthLine].text = "From youtube, credits to:";
			creditsText[seventhLine].text = "Hazel Nut and super noot ensemble";
			creditsText[eighthLine].text = "Textures and game desing";
			creditsText[ninethLine].text = "Guillermina Manzano";

			pressEnter.text = "Press enter to go back to menu";

			creditsText[firstLine].posY = screenLimit.down / 12;
			creditsText[secondLine].posY = static_cast<int>(creditsText[firstLine].posY + fontSize * 2.5f);
			creditsText[thirdLine].posY = static_cast<int>(creditsText[secondLine].posY + fontSize * 2.0f);
			creditsText[fourthLine].posY = static_cast<int>(creditsText[thirdLine].posY + fontSize * 2.0f);
			creditsText[fifthLine].posY = static_cast<int>(creditsText[fourthLine].posY + fontSize * 2.0f);
			creditsText[sixthLine].posY = static_cast<int>(creditsText[fifthLine].posY + fontSize * 2.0f);
			creditsText[seventhLine].posY = static_cast<int>(creditsText[sixthLine].posY + fontSize * 2.0f);
			creditsText[eighthLine].posY = static_cast<int>(creditsText[seventhLine].posY + fontSize * 3.0f);
			creditsText[ninethLine].posY = static_cast<int>(creditsText[eighthLine].posY + fontSize * 2.5f);

			pressEnter.posY = screenLimit.down - fontSize * 2;

			pressEnter.fontSize = static_cast<int>(25 * drawScaleY);

			ClearBackground(BLACK);

			DrawTextureEx(menuBackground.texture, { 0.0f, 0.0f }, 0.0f, static_cast<float>(screenWidth + screenHeight) /
				static_cast<float>(menuBackground.width + menuBackground.height), WHITE);

			//drawPhraseRectangle(creditsText[firstLine], YELLOW);
			DrawText(creditsText[firstLine].text, (screenWidth - MeasureText(creditsText[firstLine].text, fontSize + 10)) / 2, creditsText[firstLine].posY, fontSize + 10, MAGENTA);
			DrawText(creditsText[secondLine].text, (screenWidth - MeasureText(creditsText[secondLine].text, fontSize)) / 2, creditsText[secondLine].posY, fontSize, BLACK);
			DrawText(creditsText[thirdLine].text, (screenWidth - MeasureText(creditsText[thirdLine].text, fontSize)) / 2, creditsText[thirdLine].posY, fontSize, color);
			DrawText(creditsText[fourthLine].text, (screenWidth - MeasureText(creditsText[fourthLine].text, fontSize)) / 2, creditsText[fourthLine].posY, fontSize, color);
			DrawText(creditsText[fifthLine].text, (screenWidth - MeasureText(creditsText[fifthLine].text, fontSize)) / 2, creditsText[fifthLine].posY, fontSize, color);
			DrawText(creditsText[sixthLine].text, (screenWidth - MeasureText(creditsText[sixthLine].text, fontSize)) / 2, creditsText[sixthLine].posY, fontSize, BLACK);
			DrawText(creditsText[seventhLine].text, (screenWidth - MeasureText(creditsText[seventhLine].text, fontSize)) / 2, creditsText[seventhLine].posY, fontSize, color);
			DrawText(creditsText[eighthLine].text, (screenWidth - MeasureText(creditsText[eighthLine].text, fontSize + 10)) / 2, creditsText[eighthLine].posY, fontSize + 10, MAGENTA);
			DrawText(creditsText[ninethLine].text, (screenWidth - MeasureText(creditsText[ninethLine].text, fontSize)) / 2, creditsText[ninethLine].posY, fontSize, color);

			DrawText(pressEnter.text, (screenWidth - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, VIOLET);
		}

		void deinitialization()
		{

		}
	}
}