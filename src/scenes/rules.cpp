#include "scenes/rules.h"

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace rules
	{
		void initialization()
		{

		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
			}

			if (!mute)
				UpdateMusicStream(menuStream);
		}

		void draw()
		{
			const short fontSize = static_cast<int>(30 * drawScaleY);
			const Color color = YELLOW;

			rulesText[firstLine].text = "The objetive of the game is";
			rulesText[secondLine].text = "to eliminate all the bricks";
			rulesText[thirdLine].text = "on the screen without losing";
			rulesText[fourthLine].text = "the ball.";
			rulesText[fifthLine].text = "Controls";
			rulesText[sixthLine].text = "Good luck!";

			pressEnter.text = "Press enter to go back to menu";

			rulesText[firstLine].posY = screenLimit.down/8;
			rulesText[secondLine].posY = static_cast<int>(rulesText[firstLine].posY + fontSize * 1.5f);
			rulesText[thirdLine].posY = static_cast<int>(rulesText[secondLine].posY + fontSize * 1.5f);
			rulesText[fourthLine].posY = static_cast<int>(rulesText[thirdLine].posY + fontSize * 1.5f);
			rulesText[fifthLine].posY = static_cast<int>(rulesText[fourthLine].posY + fontSize * 3) ;
			rulesText[sixthLine].posY = static_cast<int>(rulesText[fifthLine].posY + fontSize * 3);

			pressEnter.posY = static_cast<int>(screenLimit.down - fontSize*2);

			pressEnter.fontSize = static_cast<int>(25 * drawScaleY);

			ClearBackground(BLACK);

			DrawTextureEx(menuBackground.texture, { 0.0f, 0.0f }, 0.0f, static_cast<float>(screenWidth + screenHeight) / 
				static_cast<float>(menuBackground.width + menuBackground.height), WHITE);

			DrawText(rulesText[firstLine].text, (screenWidth - MeasureText(rulesText[firstLine].text, fontSize))/ 2, rulesText[firstLine].posY, fontSize, color);
			DrawText(rulesText[secondLine].text, (screenWidth - MeasureText(rulesText[secondLine].text, fontSize)) / 2, rulesText[secondLine].posY, fontSize, color);
			DrawText(rulesText[thirdLine].text, (screenWidth - MeasureText(rulesText[thirdLine].text, fontSize)) / 2, rulesText[thirdLine].posY, fontSize, color);
			DrawText(rulesText[fourthLine].text, (screenWidth - MeasureText(rulesText[fourthLine].text, fontSize)) / 2, rulesText[fourthLine].posY, fontSize, color);
			DrawText(rulesText[fifthLine].text, (screenWidth - MeasureText(rulesText[fifthLine].text, fontSize)) / 2, rulesText[fifthLine].posY, fontSize, color);

			DrawText(FormatText("%c - ", player.controls.left), 
					(screenWidth - (MeasureText(FormatText("%c - ", player.controls.left), fontSize)) - (MeasureText(FormatText(" %c", player.controls.right), fontSize))) / 2, 
					rulesText[fifthLine].posY + fontSize, fontSize, color);
			DrawText(FormatText(" %c", player.controls.right), 
					(screenWidth + (MeasureText(FormatText("%c", player.controls.right), fontSize))) / 2,
					rulesText[fifthLine].posY + fontSize, fontSize, color);

			DrawText(rulesText[sixthLine].text, (screenWidth - MeasureText(rulesText[sixthLine].text, fontSize)) / 2, rulesText[sixthLine].posY, fontSize, color);

			DrawText(pressEnter.text, (screenWidth- MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, VIOLET);
		}

		void deinitialization()
		{
			
		}
	}
}