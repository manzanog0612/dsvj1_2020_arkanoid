#include "scenes/menu.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace menu
	{
		static void drawingVarsInitialization()
		{
			play.text = "Play";
			rules.text = "Rules";
			credits.text = "Credits";
			exit.text = "Exit";

			play.fontSize = static_cast<int>(50 * drawScaleY);
			rules.fontSize = play.fontSize;
			credits.fontSize = play.fontSize;
			exit.fontSize = play.fontSize;
			title.fontSize = play.fontSize*2;

			play.posY = screenHeight - play.fontSize * 6;
			rules.posY = screenHeight - play.fontSize * 4 - play.fontSize / 2;
			credits.posY = screenHeight - play.fontSize * 3;
			exit.posY = screenHeight - play.fontSize - play.fontSize / 2;
			title.posY = static_cast<int>(play.fontSize * 1.5f);
		}

		static void setClickSound()
		{
			selectOption = true;
			soundPlayed = true;
		}

		static void highlightMenuOption()
		{
			if (mouse.posY >= play.posY && mouse.posY <= play.posY + play.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(play.text, play.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(play.text, play.fontSize)) / 2 + MeasureText(play.text, play.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::GAMEPLAY;
				if (!soundPlayed)
					setClickSound();
			}
			else if (mouse.posY >= rules.posY && mouse.posY <= rules.posY + rules.fontSize &&
					mouse.posX >= (screenWidth - MeasureText(rules.text, rules.fontSize)) / 2 &&
					mouse.posX <= (screenWidth - MeasureText(rules.text, rules.fontSize)) / 2 + MeasureText(rules.text, rules.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::RULES;
				if (!soundPlayed)
					setClickSound();
			}
			else if (mouse.posY >= credits.posY && mouse.posY <= credits.posY + credits.fontSize &&
					mouse.posX >= (screenWidth - MeasureText(credits.text, credits.fontSize)) / 2 &&
					mouse.posX <= (screenWidth - MeasureText(credits.text, credits.fontSize)) / 2 + MeasureText(credits.text, credits.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::CREDITS;
				if (!soundPlayed)
					setClickSound();
			}
			else if (mouse.posY >= exit.posY && mouse.posY <= exit.posY + exit.fontSize &&
					mouse.posX >= (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2 &&
					mouse.posX <= (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2 + MeasureText(exit.text, exit.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::EXIT;
				if (!soundPlayed)
					setClickSound();
			}
			else
			{
				futurePlaceInGame = PLACEINGAME::NONE;
				if (soundPlayed)
				{
					selectOption = false;
					soundPlayed = false;
				}
				
			}
		}

		static void goToPlaceSelected()
		{
			if (mouse.posY >= play.posY && mouse.posY <= play.posY + play.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(play.text, play.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(play.text, play.fontSize)) / 2 + MeasureText(play.text, play.fontSize))
			{
				currentPlaceInGame = PLACEINGAME::GAMEPLAY;
			}
			else if (mouse.posY >= rules.posY && mouse.posY <= rules.posY + rules.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(rules.text, rules.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(rules.text, rules.fontSize)) / 2 + MeasureText(rules.text, rules.fontSize))
			{
				currentPlaceInGame = PLACEINGAME::RULES;
			}
			else if (mouse.posY >= credits.posY && mouse.posY <= credits.posY + credits.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(credits.text, credits.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(credits.text, credits.fontSize)) / 2 + MeasureText(credits.text, credits.fontSize))
			{
				currentPlaceInGame = PLACEINGAME::CREDITS;
			}
			else if (mouse.posY >= exit.posY && mouse.posY <= exit.posY + exit.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2 + MeasureText(exit.text, exit.fontSize))
			{
				currentPlaceInGame = PLACEINGAME::EXIT;
			}
			else
			{
				menuOptionChosen = false;
			}
		}

		static void setEnterSound()
		{
			enterPressed = true;
			soundPlayed = true;
		}

		static void drawGameVersion()
		{
			gameVersion.text = "v0.1";
			gameVersion.fontSize = static_cast<int>(25 * drawScaleY);
			gameVersion.posY = screenLimit.down - gameVersion.fontSize - contourLineThickness;
			
			DrawText(gameVersion.text, (screenLimit.right - contourLineThickness - MeasureText(gameVersion.text, gameVersion.fontSize)),
				gameVersion.posY, gameVersion.fontSize, YELLOW);
		}

		//--------------------------------------------------------------

		void initialization()
		{
			returnToMenu = false;
			menuOptionChosen = false;
			currentPlaceInGame = PLACEINGAME::MENU;
			futurePlaceInGame = PLACEINGAME::MENU;
			drawingVarsInitialization();
		}

		void input()
		{
			mouse.posX = GetMouseX();
			mouse.posY = GetMouseY();

			if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				menuOptionChosen = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			highlightMenuOption();

			if (!inMenu)
			{
				inMenu = true;
				playMusic();
			}
			if (!mute)
			{
				UpdateMusicStream(menuStream);
			}
			playSound();
			if (selectOption)	selectOption = false;
			if (enterPressed)	enterPressed = false;

			if (menuOptionChosen)
			{
				goToPlaceSelected();
				menuOptionChosen = false;
				setEnterSound();
				soundPlayed = false;
				inMenu = false;
			}
			
		}

		void draw()
		{
			const Color color = GREEN;
			const Color selected = MAGENTA;

			ClearBackground(BLACK);

			DrawTextureEx(menuBackground.texture, { 0.0f, 0.0f }, 0.0f, static_cast<float>(screenWidth + screenHeight) / static_cast<float>(menuBackground.width + menuBackground.height), WHITE);
			
			drawGameVersion();

			DrawText(title.text, (screenWidth - MeasureText(title.text, title.fontSize)) / 2, title.posY, title.fontSize, BLACK);

			switch (futurePlaceInGame)
			{
			case configurations::PLACEINGAME::GAMEPLAY:
				DrawText(play.text, (screenWidth - MeasureText(play.text, play.fontSize)) / 2, play.posY, play.fontSize, selected);
				DrawText(rules.text, (screenWidth - MeasureText(rules.text, rules.fontSize)) / 2, rules.posY, rules.fontSize, color);
				DrawText(credits.text, (screenWidth - MeasureText(credits.text, credits.fontSize)) / 2, credits.posY, credits.fontSize, color);
				DrawText(exit.text, (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, color);
				break;
			case configurations::PLACEINGAME::RULES:
				DrawText(play.text, (screenWidth - MeasureText(play.text, play.fontSize)) / 2, play.posY, play.fontSize, color);
				DrawText(rules.text, (screenWidth - MeasureText(rules.text, rules.fontSize)) / 2, rules.posY, rules.fontSize, selected);
				DrawText(credits.text, (screenWidth - MeasureText(credits.text, credits.fontSize)) / 2, credits.posY, credits.fontSize, color);
				DrawText(exit.text, (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, color);
				break;
			case configurations::PLACEINGAME::CREDITS:
				DrawText(play.text, (screenWidth - MeasureText(play.text, play.fontSize)) / 2, play.posY, play.fontSize, color);
				DrawText(rules.text, (screenWidth - MeasureText(rules.text, rules.fontSize)) / 2, rules.posY, rules.fontSize, color);
				DrawText(credits.text, (screenWidth - MeasureText(credits.text, credits.fontSize)) / 2, credits.posY, credits.fontSize, selected);
				DrawText(exit.text, (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, color);
				break;
			case configurations::PLACEINGAME::EXIT:
				DrawText(play.text, (screenWidth - MeasureText(play.text, play.fontSize)) / 2, play.posY, play.fontSize, color);
				DrawText(rules.text, (screenWidth - MeasureText(rules.text, rules.fontSize)) / 2, rules.posY, rules.fontSize, color);
				DrawText(credits.text, (screenWidth - MeasureText(credits.text, credits.fontSize)) / 2, credits.posY, credits.fontSize, color);
				DrawText(exit.text, (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, selected);
				break;
			case configurations::PLACEINGAME::NONE:
			default:
				DrawText(play.text, (screenWidth - MeasureText(play.text, play.fontSize)) / 2, play.posY, play.fontSize, color);
				DrawText(rules.text, (screenWidth - MeasureText(rules.text, rules.fontSize)) / 2, rules.posY, rules.fontSize, color);
				DrawText(credits.text, (screenWidth - MeasureText(credits.text, credits.fontSize)) / 2, credits.posY, credits.fontSize, color);
				DrawText(exit.text, (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, color);
				break;
			}
		}

		void deinitialization()
		{

		}
	}
}