#ifndef RULES_H
#define RULES_H

namespace game
{
	namespace rules
	{
		void initialization();

		void input();

		void update();

		void draw();

		void deinitialization();
	}
}

#endif // RULES_H
