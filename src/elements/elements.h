#ifndef ELEMENTS_H
#define ELEMENTS_H

#include "raylib.h"

#include "elements_properties.h"

using namespace game;
using namespace elements_properties;

namespace game
{
	namespace elements
	{
		namespace pad
		{
			struct Pad
			{
				Rectangle pad;
				Color color;
				Controls controls;
				short speed;
				short lives;
				short score = 0;
			};
		}

		namespace bricks_
		{
			struct Brick
			{
				Rectangle brick;
				Color color;
				short hits;
				bool active;
				TYPESOFBRIKS type;
			};

			const short bricksPerLine = 5;
			const short linesOfBricks = 8;
		}

		namespace ball_
		{
			struct Ball
			{
				Vector2 center;
				float radius;
				Color color;
				State state;
				Vector2 speed;
				bool active;
			};
		}
	}
}
#endif //ELEMENTS_H