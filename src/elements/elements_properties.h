#ifndef ELEMENTS_PROPERTIES_H
#define ELEMENTS_PROPERTIES_H

namespace game
{
	namespace elements_properties
	{
		enum class COLOR
		{
			_GRAY = 1, _DARKGRAY, _YELLOW, _GOLD, _ORANGE, _PINK, _RED, _MAROON, _GREEN, _DARKGREEN, _SKYBLUE,
			_BLUE, _DARKBLUE, _VIOLET, _DARKPURPLE, _BEIGE, _BROWN, _DARKBROWN, _WHITE, _BLACK, _MAGENTA, _RAYWHITE
		};

		enum class STATEOFBALL { ONCOLLISION, NOTONCOLLISION };

		struct State
		{
			STATEOFBALL actual;
			STATEOFBALL previous;
		};

		struct Controls
		{
			int left;
			int right;
		};

		enum class TYPESOFBRIKS { BREAKABLE, UNBREAKABLE, NONEXISTENT };
	}
}

#endif // ELEMENTS_PROPERTIES_H