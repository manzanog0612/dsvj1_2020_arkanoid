#ifndef GLOBAL_DRAWING_VARS_H
#define GLOBAL_DRAWING_VARS_H

#include "raylib.h"

namespace game
{
	namespace global_drawing_vars
	{
		struct Words
		{
			const char* text;
			int posY;
			int  fontSize;
		};

		extern float originalScreenWidth;
		extern float originalScreenHeight;

		extern float drawScaleX;
		extern float drawScaleY;

		extern Words title;

		extern Words play;
		extern Words rules;
		extern Words credits;
		extern Words exit;

		extern Words rulesText[8];
		extern Words creditsText[10];

		extern int firstLine;
		extern int secondLine;
		extern int thirdLine;
		extern int fourthLine;
		extern int fifthLine;
		extern int sixthLine;
		extern int seventhLine;
		extern int eighthLine;
		extern int ninethLine;
		extern int tenthLine;

		extern Words pressEnter;
		extern Words pressP;
		extern Words pressR;
		extern Words pressSpace;
		extern Words pressM;

		extern Words score;

		extern Words pause;

		extern void drawPhraseRectangle(Words word, Color color);

		extern Words loser[2];
		extern Words winner[2];

		extern Words youWon;
		extern Words congratulations;
		extern Words youLost;

		extern Words gameVersion;
	}
}

#endif //GLOBAL_DRAWING_VARS_H
