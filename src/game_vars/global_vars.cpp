#include "game_vars/global_vars.h"

#include "elements/elements.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"

using namespace game;
using namespace elements;
using namespace global_drawing_vars;
using namespace configurations;

namespace game
{
	namespace global_vars
	{
		pad::Pad player;

		bricks_::Brick bricks[bricks_::linesOfBricks][bricks_::bricksPerLine];

		ball_::Ball ball;

		short fps = 60;

		short padMovement = 0;

		PLACEINGAME currentPlaceInGame;
		PLACEINGAME futurePlaceInGame;

		Mouse mouse;

		bool menuOptionChosen = false;

		bool returnToMenu = false;

		bool pauseGame = false;

		bool restart = false;

		short contourLineThickness = static_cast<short>(12 * drawScaleX);

		bool win = false;

		bool playinGame = true;
	}
}
