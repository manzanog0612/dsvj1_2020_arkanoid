#include "game_vars/global_drawing_vars.h"

#include "raylib.h"

#include "configurations/configurations.h"

using namespace game;
using namespace configurations;

namespace game
{
	namespace global_drawing_vars
	{
		float originalScreenWidth = 500;
		float originalScreenHeight = 600;

		float drawScaleX = screenWidth / originalScreenWidth;
		float drawScaleY = screenHeight / originalScreenHeight;

		Words title;

		Words play;
		Words rules;
		Words credits;
		Words exit;

		Words rulesText[8];
		Words creditsText[10];

		int firstLine = 0;
		int secondLine = 1;
		int thirdLine = 2;
		int fourthLine = 3;
		int fifthLine = 4;
		int sixthLine = 5;
		int seventhLine = 6;
		int eighthLine = 7;
		int ninethLine = 8;
		int tenthLine = 9;

		Words pressEnter;
		Words pressP;
		Words pressR;
		Words pressSpace;
		Words pressM;

		Words score;

		Words pause;

		void drawPhraseRectangle(Words word, Color color)
		{
			Rectangle rec;

			rec.x = static_cast<float>((screenLimit.right - MeasureText(word.text, word.fontSize)) / 2 - 5);
			rec.y = static_cast<float>(word.posY - 4);
			rec.width = MeasureText(word.text, word.fontSize) + 10;
			rec.height = word.fontSize + 8;

			DrawRectangleRec(rec, color);
		}

		Words loser[2];
		Words winner[2];

		Words youWon;
		Words congratulations;
		Words youLost;

		Words gameVersion;
	}
}