#ifndef GLOBAL_VARS_H
#define GLOBAL_VARS_H

#include "raylib.h"

#include "elements/elements.h"
#include "configurations/configurations.h"

using namespace game;
using namespace elements;
using namespace configurations;

namespace game
{
	namespace global_vars
	{
		extern pad::Pad player;

		extern bricks_::Brick bricks[bricks_::linesOfBricks][bricks_::bricksPerLine];

		extern ball_::Ball ball;

		extern short fps;

		extern short padMovement;

		extern PLACEINGAME currentPlaceInGame;
		extern PLACEINGAME futurePlaceInGame;

		extern Mouse mouse;

		extern bool menuOptionChosen;

		extern bool returnToMenu;

		extern bool pauseGame;

		extern bool restart;

		extern short contourLineThickness;

		extern bool win;

		extern bool playinGame;
	}
}
#endif //GLOBAL_VARS_H
